<h1 style="margin:0">"Baron's Picnic" project.</h1>
<h2>Come to our table and eat...</h2>

<br />

*Note: This branch is for developing purposes.*

<br />

This project is bundle of personal and/or comercial automatization modules for [Microsoft's PowerShell](https://docs.microsoft.com/en-us/powershell/ "Official PowerShell documentation.").

## Our table contains: ##

  * [Toasts.](./BaronsToasts/0.0.1/ "Toast notification handler.")

<br />

## Social life ##
Email : [baronprosimir@gmail.com](mailto:baronprosimir@gmail.com "My mail address.")  
Facebook : [BaronProsimir](https://facebook.com/BaronProsimir "Official Facebook page.")