function Send-BP-Toast {
  <#
  .SYNOPSIS
      Send user-defined Toast notification. 
  .DESCRIPTION
      This modul is made for sending user-defined Toast notifications on Windows machines.
      As a source of the Toast notification, module using XML file you have to pass as argument...
      
      For more information about Toast notifications, visit the official Microsoft documentation webpage:
      https://docs.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/toast-ux-guidance
  .EXAMPLE
      Send-BP-Toast -FilePath "C:\PROJECTS\PowerShell_scripts\Toast_notifications\Toasts_XML_files\" -FileName "MyToastBody.xml"
  .EXAMPLE
      Send-BP-Toast -FileFullPath "C:\PROJECTS\PowerShell_scripts\Toast_notifications\Toasts_XML_files\MyToastBody.xml"
  .INPUTS
    Strings
  .OUTPUTS
    Windows Toast notification [Windows.UI.Notifications]
  .NOTES
      Author: BaronProsimir
      GIT: https://gitlab.com/BaronProsimir
      Module web:
      Webside: https://twitch.tv/baronprosimir
  #>
  [CmdletBinding()]
  [Alias("bpts")]
  param (
    # Full path to Toast XML source file. {Default: CURRENT_DIRECTORY\XmlSources\ToastXml.xml}
    [Parameter(Position=0, ValueFromPipelineByPropertyName=$true)][Alias("ffph", "ffp")]
    [string]$FileFullPath = "$PSScriptRoot\XmlSources\ToastXml.xml",

    # Path to Toast XML source file. {Default: CURRENT_DIRECTORY\XmlSources}
    [Parameter(Position=1, ValueFromPipelineByPropertyName=$true)][ValidateNotNullOrEmpty()][Alias("fph", "fp")]
    [String]$FilePath = "$PSScriptRoot\XmlSources",
    
    # Source XML file name. {Default: "ToastSource.xml"}
    # (https://docs.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/adaptive-interactive-toasts)
    [Parameter(Position=2, ValueFromPipelineByPropertyName=$true)][ValidateNotNullOrEmpty()][Alias("fnm", "fn")]
    [String]$FileName = "ToastXml.xml",
    
    # Group identifier of current Toast notification.
    # (https://docs.microsoft.com/en-us/uwp/api/windows.ui.notifications.toastnotification.group)
    [Parameter(Position=3, ValueFromPipelineByPropertyName=$true)][Alias("g", "grp")]
    [String]$Group = "DefaultGroup",
    
    # Tag of current Toast notification.
    # (https://docs.microsoft.com/en-us/uwp/api/windows.ui.notifications.toastnotification.tag)
    [Parameter(Position=4, ValueFromPipelineByPropertyName=$true)][Alias("t", "tg")]
    [String]$Tag = "DefaultTag",

    # AppID (AUMID).
    # Application, from which notification will be send. {Default is Excel}
    # IMPORTANT NOTE: To send notification from application, you have to have INSTALL THIS APPLICATION ON YOUR COMPUTER!
    #
    # To find out Application ID, run 'Get-StartApps' in Powershell.
    # For more details check: 
    # https://docs.microsoft.com/en-us/windows/configuration/find-the-application-user-model-id-of-an-installed-app

    [Parameter(Position=5, ValueFromPipelineByPropertyName=$true)][Alias("app")]
    [string]$ApplicationFrom = "Microsoft.Office.EXCEL.EXE.15"
  )

  begin {
    # Set default namespaces.
    $null = [Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime];
    $null = [Windows.Data.Xml.Dom.XmlDocument, Windows.Data.Xml.Dom.XmlDocument, ContentType = WindowsRuntime];
    # Application, from which notification was sent. [Excel]:
    $private:app = $ApplicationFrom; 
    # Create new XML object:
    $private:xdoc = New-Object System.Xml.XmlDocument;
    # Create new DOM-XML object:
    $private:xml = New-Object Windows.Data.Xml.Dom.XmlDocument;
    
  }

  process {
    # Add XML extension if missing:
    if ($FileName.Substring($FileName.length - 4, 4) -ne ".xml") { $FileName = $FileName + ".xml" };
    # Load content of file into XML object:
    $private:xdoc.Load("$FileFullPath");
    # Load XML into DOM-XML object:
    $private:xml.LoadXml($private:xdoc.OuterXml);
    # Create new Toast object with XML from file:
    $private:Toast = [Windows.UI.Notifications.ToastNotification]::new($private:xml);

    # Set Toast tag:
    $private:Toast.Tag = $Tag;
    # Set Toast group:
    $private:Toast.Group = $Group;
  }
  end {
    # Show notification from $app aplication.
    [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier($private:app).Show($private:Toast);
  }
}

function Update-BP-Toast {
  [CmdletBinding()]
  param (
    # Path to Toast XML source file. {Default: Current directory.}
    [ValidateNotNullOrEmpty()]
    [String]$FilePath = "$PSScriptRoot\XmlSources",
    # Source XML file name. {Default: "ToastSource.xml"}
    [ValidateNotNullOrEmpty()]
    [String]$FileName = "ToastXml_update.xml",
    [String]$Group = "DefaultGroup",
    [String]$Tag = "DefaultTag"
  )
  # Function body #
  # Set default namespaces.
  $null = [Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime]
  $null = [Windows.Data.Xml.Dom.XmlDocument, Windows.Data.Xml.Dom.XmlDocument, ContentType = WindowsRuntime]
  $null = [Windows.UI.Notifications.NotificationData, Windows.UI.Notifications.NotificationData, ContentType = WindowsRuntime]

  # NOTE: Displays PowerShell Header and icon. #
  $private:app = "Microsoft.Office.EXCEL.EXE.15" # Application, from which notification was sent. [Excel]
  # Create new XML object.
  $private:xdoc = New-Object System.Xml.XmlDocument
  # Add XML extension if missing.
  if ($FileName.Substring($FileName.length - 4, 4) -ne ".xml") { $FileName = $FileName + ".xml" }
  # Load content of file into XML object.
  $private:xdoc.Load("$FilePath\$FileName")
  # Create new DOM-XML object.
  $private:xml = New-Object Windows.Data.Xml.Dom.XmlDocument
  # Load XML into DOM-XML object.
  $private:xml.LoadXml($private:xdoc.OuterXml)
  $dDic = New-Object "system.collections.generic.dictionary[string,string]"
  $dDic.Add("#text","GETAFE2")
  # Create new Toast object with XML from file.
  $private:ToastData = [Windows.UI.Notifications.NotificationData]::new($dDic)
  # Show notification from $app aplication.
  [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier($private:app).Update($private:ToastData, $private:Toast.Tag, $private:Toast.Group)
}

function New-BP-ToastXMLSource {
  [CmdletBinding()]
  param (
    # Highlighted text on first row.
    [Parameter()]
    [Alias("th")]
    [string]
    $TextHighlighted,
    # Non-highlighted text on second row
    [Parameter()]
    [Alias("t1")]
    [string]
    $TextLine1,
    # Nonhighlighted text on thirth row.
    [Parameter()]
    [Alias("t2")]
    [string]
    $TextLine2,
    # Small text right next to the time.
    [Parameter()]
    [Alias("ta")]
    [string]
    $TextAttribution,
    # Path where XML source file will be created.
    [Parameter()]
    [Alias("p")]
    [string]
    $Path = [Environment]::GetFolderPath("MyDocuments") + "\WindowsPowerShell\Modules\ToastsFromExcel\0.0.1\ToastXml_FromExcel.xml"
  )
  $newNB = BuildNotificationBody;
  return $newNB
}

Function BuildNotificationBody{
  ### DESTCRIPTION: Build final notification's XML source file. ###
  $bnb = "";
  $bnb = "<toast>`n";
  $bnb = $bnb + "`t <visual>`n";
  $bnb = $bnb + "`t`t <binding template = ""ToastGeneric"">`n";
  # If AddImage Then $bnb = $bnb "`t`t "<image " + _
    # "placement=""" + Switch(img_placement = AppLogoOverride, "appLogoOverride", img_placement = Hero, "Hero", img_placement = Inline, "Inline") + _
    # """ hint-crop=""" + Switch(img_hCrop = def, "Default", img_hCrop = InCircle, "Circle", img_hCrop = None, "None") + _
    # """ src=""" + img_src + """ />"
  $bnb = $bnb + "`t`t`t <text hint-maxLines=""4"">" + $th + "</text>`n";
  $bnb = $bnb + "`t`t`t <text>" + $t1 + "</text>`n";
  $bnb = $bnb + "`t`t`t <text>" + $t2 + "</text>`n";
  $bnb = $bnb + "`t`t`t <text placement=""attribution"">" + $ta + "</text>`n";
  # If AddProgress Then $bnb = $bnb "`t`t`t "<progress status = """ + pb_status + """ value = """ + Replace(pb_value, ",", ".") + """ title = """ + pb_title + """ valueStringOverride = """ + pb_valueStringOverride + """ />"
  $bnb = $bnb + "`t`t </binding>`n";
  $bnb = $bnb + "`t </visual>`n";
  # If SoundSelected Then $bnb = $bnb "`t "<audio src = """ + s + """ />"
  $bnb = $bnb + "</toast>`n";
  return bnb
}

Export-ModuleMember -Alias "bpts"
export-ModuleMember -Function "Send-BP-Toast" #, "Update-BP-Toast" #, "New-BP-ToastXMLSource"