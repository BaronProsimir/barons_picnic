@{
  RootModule = 'BaronsToasts.psm1'
  ModuleVersion = "0.0.1"
  GUID = "95356ae7-ac74-478c-84aa-64ce3266ee20"
  Author = "BaronProsimir"
  CompanyName = "Katoen Natie"
  Copyright = "GNU GPLv3"
  Description = "This module allows users to send Toast notifications from PowerShell using XML file as it's source."
  PowerShellVersion = "5.1"
  AliasesToExport = @("bpts")
  FunctionsToExport = 
    "Send-BP-Toast"
    # "Update-BP-Toast"
    # "New-BP-ToastXMLSource"
  PrivateData = @{
    PSdata = @{
      Tags = @("Powershell", "BaronProsimir", "ScriptingBarons", "GNU", "Automatization", "ToastNotifications", "GUI")
      LicenseUri = "www.gnu.org/licenses/gpl.html"
      ReleaseNotes =
      "
        # Version 0.0.1 #
          * Default module members created.
      "
    }
  }

}